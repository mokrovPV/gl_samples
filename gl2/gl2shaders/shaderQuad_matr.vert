
#version 330

layout(location = 0) in vec2 vertexPosition;

uniform mat3 posmatrix;
uniform mat3 colmatrix;

//out vec2 position;

void main()
{
    vec3 postransformed = posmatrix * vec3(vertexPosition.yx,1);

//    gl_Position = vec4(postransformed.yx / postransformed.z, 0, 1);
    gl_Position = vec4(postransformed.yx, -postransformed.z, postransformed.z);
}
