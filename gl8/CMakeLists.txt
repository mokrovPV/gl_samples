set(SRC_FILES	
	${PROJECT_SOURCE_DIR}/common/Application.cpp
	${PROJECT_SOURCE_DIR}/common/DebugOutput.cpp
	${PROJECT_SOURCE_DIR}/common/Camera.cpp
	${PROJECT_SOURCE_DIR}/common/Framebuffer.cpp
	${PROJECT_SOURCE_DIR}/common/Mesh.cpp
	${PROJECT_SOURCE_DIR}/common/ShaderProgram.cpp
	${PROJECT_SOURCE_DIR}/common/Texture.cpp
	${PROJECT_SOURCE_DIR}/common/QueryObject.cpp
	${PROJECT_SOURCE_DIR}/common/ConditionalRender.cpp
)

set(HEADER_FILES
	${PROJECT_SOURCE_DIR}/common/Application.hpp
	${PROJECT_SOURCE_DIR}/common/DebugOutput.h
	${PROJECT_SOURCE_DIR}/common/Camera.hpp
	${PROJECT_SOURCE_DIR}/common/Framebuffer.hpp
	${PROJECT_SOURCE_DIR}/common/Mesh.hpp
	${PROJECT_SOURCE_DIR}/common/ShaderProgram.hpp
	${PROJECT_SOURCE_DIR}/common/Texture.hpp
	${PROJECT_SOURCE_DIR}/common/QueryObject.h
	${PROJECT_SOURCE_DIR}/common/ConditionalRender.h
)

MAKE_SAMPLE(gl8_Grayscale)
MAKE_SAMPLE(gl8_Gamma)
MAKE_SAMPLE(gl8_HDR)
MAKE_SAMPLE(gl8_SSAO)
MAKE_SAMPLE(gl8_DoF)
MAKE_SAMPLE(gl8_Timer_Query_in_HDR)

COPY_RESOURCE(gl8shaders)

add_dependencies(gl8_Grayscale gl8shaders)
add_dependencies(gl8_Gamma gl8shaders)
add_dependencies(gl8_HDR gl8shaders)
add_dependencies(gl8_SSAO gl8shaders)
add_dependencies(gl8_DoF gl8shaders)
add_dependencies(gl8_Timer_Query_in_HDR gl8shaders)
