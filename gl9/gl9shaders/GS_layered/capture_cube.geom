#version 330
#extension GL_ARB_gpu_shader5 : enable

layout(triangles, invocations=6) in;
layout(triangle_strip, max_vertices=3) out;

in VS_OUT {
    vec3 normalWorldSpace;
    vec2 texCoord;
} vsout[];

out vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
out vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
out vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

uniform mat4 P; // Projection matrix for cube faces.
uniform mat4 faceV[6]; // View matrices for each face of the cube.
uniform mat3 faceV_normals[6]; // view matrices for each face of the cube for normals transformation.

#define LAYER_X_POS 0
#define LAYER_X_NEG 1
#define LAYER_Y_POS 2
#define LAYER_Y_NEG 3
#define LAYER_Z_POS 4
#define LAYER_Z_NEG 5

// Deduced from inverse cubemap texture matrix.
// Should be set as uniform.
const int invocations_mapping[6] = int[6]( LAYER_Y_POS, LAYER_Y_NEG, LAYER_Z_POS, LAYER_Z_NEG, LAYER_X_NEG, LAYER_X_POS );

// Determine on which face of cube direction v is projected.
int getLayer(vec3 v) {
    vec3 vabs = abs(v);
	bvec3 a = greaterThan(vabs, vabs.yzx);
	bvec3 b = greaterThan(vabs, vabs.zxy);
	ivec3 combo = ivec3(a)*ivec3(b);
    combo = combo << ivec3(0,1,2);
    int bit = findLSB(combo.x + combo.y + combo.z);
    int base = 2 * bit;
    int negLayer = int(v[bit] < 0);
    return base+negLayer;
}

#define EMIT_VERTEX(id) \
    gl_Layer = gl_InvocationID; \
    gl_Position = positionsProjSpace[id]; \
    normalCamSpace = normals[id]; \
    posCamSpace = positionsCamSpace[id]; \
    texCoord = vsout[id].texCoord; \
    EmitVertex(); \
// EMIT_VERTEX over

void main() {
    int layer0 = getLayer(gl_in[0].gl_Position.xyz + faceV[gl_InvocationID][3].xyz);
    int layer1 = getLayer(gl_in[1].gl_Position.xyz + faceV[gl_InvocationID][3].xyz);
    int layer2 = getLayer(gl_in[2].gl_Position.xyz + faceV[gl_InvocationID][3].xyz);

    if (layer0 == layer1 && layer1 == layer2 && layer2 != invocations_mapping[gl_InvocationID]) {
        return;
    }

    mat3x4 positionsCamSpace = faceV[gl_InvocationID] * mat3x4(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_in[2].gl_Position);
    mat3x4 positionsProjSpace = P * positionsCamSpace;
    mat3x3 normals = faceV_normals[gl_InvocationID] * mat3x3(vsout[0].normalWorldSpace, vsout[1].normalWorldSpace, vsout[2].normalWorldSpace);

    EMIT_VERTEX(0);
    EMIT_VERTEX(1);
    EMIT_VERTEX(2);

    EndPrimitive();
}
