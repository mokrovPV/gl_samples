#version 330

uniform sampler2D tex;

in vec2 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

uniform vec2 center;
uniform float alpha;
uniform vec4 colorFactor;

vec2 normalizeSafe(vec2 v) {
    if (dot(v,v) < 1e-5f)
        return vec2(0);
    return normalize(v);
}

void main() {
    vec2 coords = center + normalizeSafe(texCoord-center) * pow(length(texCoord - center), alpha);
    fragColor = texture(tex, coords) * colorFactor;
}
